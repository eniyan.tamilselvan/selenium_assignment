package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SeleniumTestng {
	public static WebDriver driver;

	@BeforeClass
	public void openbrower() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");

	}

	@Test(priority = 1)
	public void login() {
		driver.findElement(By.linkText("Log in")).click();
		driver.findElement(By.id("Email")).sendKeys("eniyan24@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Eniyan@123");
		driver.findElement(By.id("RememberMe")).click();
		driver.findElement(By.xpath("//input[@class='button-1 login-button']")).click();
	}

	@Test(priority = 2)
	public void ShortbyProduct() {
		WebElement findElement = driver
				.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Electronics')]"));

		Actions as = new Actions(driver);
		as.moveToElement(findElement).build().perform();
		as.moveToElement(driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Cell phones')]")))
				.click().perform();

	}

	@Test(dependsOnMethods = "ShortbyProduct")
	public void Addcar_prouduct() {
		driver.findElement(By.xpath("(//input[@type='button'])[2]")).click();
		driver.findElement(By.partialLinkText("Shopping cart")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();

	}

	@AfterClass
	public void logout() {
		driver.findElement(By.partialLinkText("Log out")).click();
		driver.close();

	}

}
